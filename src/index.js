new gridjs.Grid({
    columns: ['No','Name', 'Username', 'Email','Address', 'Company'],
    server: {
        url: 'https://jsonplaceholder.typicode.com/users',
        then: data => data.map((res,i) => [i+1, res.name, res.username, res.email, (res.address.street +' '+ res.address.suite + ' '+ res.address.city)  , res.company.name, null])
    },

}).render(document.getElementById("gridtable"));

